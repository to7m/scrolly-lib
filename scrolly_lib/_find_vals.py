import numpy as np
import scipy.optimize as opt


RNG = np.random.default_rng(0)


def print_arr_like_list(arr):
    print('[', ", ".join('{:.4f}'.format(x) for x in arr), ']', sep='')


def find_vals(length, scorer, displayer):
    if 0:
        start = RNG.random(length, dtype=np.float32)
        border_vels = opt.minimize(
            lambda x: scorer.get_scores(x).sum(),
            start,
            method="Nelder-Mead",
            options={"maxiter": 5000}
        ).x
    else:
        border_vels = np.asarray(
            [0.05, 0.0976, 0.0436, 0.0410, 0.0084, 0.0957, 0.1395, 0.01],
            dtype=np.float32
        )

    print_arr_like_list(border_vels)
    print_arr_like_list(scorer.get_scores(border_vels))
    print(scorer.get_scores(border_vels).sum())
    displayer.display(border_vels)
    return border_vels
