import numpy as np

from ._find_vals import find_vals


_MIN_VEL_AS_MUL_OF_AVG_VEL = 0.1


class _Segment:
    def __init__(self, start_t, stop_t, a, b, c):
        self.start_t, self.stop_t = start_t, stop_t
        self.a, self.b, self.c = a, b, c


class _PageCalculator:
    def __init__(self, dur, w, in_vel, out_vel):
        self._dur, self._w = dur, w

        self._avg_vel = w / dur
        self._min_vel = self._avg_vel * _MIN_VEL_AS_MUL_OF_AVG_VEL

        self._in_vel = max(in_vel, self._min_vel)
        self._out_vel = max(out_vel, self._min_vel)

        self._segments = self._calculate_segments()
        self.max_abs_accel = max(abs(segment.a) for segment in self._segments)

    def _calculate_2_segments(self):
        avg_vel_without_shift = (self._in_vel + self._out_vel) / 2
        vel_increase_at_shift = 2 * (self._avg_vel - avg_vel_without_shift)
        vel_increase_is_positive = vel_increase_at_shift >= 0

        default_accel = (self._out_vel - self._in_vel) / self._dur

        shift_t = self._dur / 2
        prev_adjustment = 0.0
        adjustment = shift_t / 2
        while adjustment != prev_adjustment:
            unadjusted_shift_vel = self._in_vel + shift_t * default_accel
            shift_vel = unadjusted_shift_vel + vel_increase_at_shift
            in_accel = (shift_vel - self._in_vel) / shift_t
            out_accel = (self._out_vel - shift_vel) / (self._dur - shift_t)

            if vel_increase_is_positive:
                should_delay_shift = in_accel + out_accel >= 0
            else:
                should_delay_shift = in_accel + out_accel < 0

            if should_delay_shift:
                shift_t += adjustment
            else:
                shift_t -= adjustment

            prev_adjustment = adjustment
            adjustment *= 0.5

        shift_x = (self._in_vel + shift_vel) / 2 * shift_t

        return [
            _Segment(
                start_t=0.0, stop_t=shift_t,
                a=in_accel, b=self._in_vel, c=0.0
            ),
            _Segment(
                start_t=shift_t, stop_t=self._dur,
                a=out_accel, b=shift_vel, c=shift_x
            )
        ]

    def _calculate_3_segments(self):
        in_above_min_vel = self._in_vel - self._min_vel
        out_above_min_vel = self._out_vel - self._min_vel

        w_remaining_after_min_vel = self._w - self._min_vel * self._dur
        w_covered_by_abs_accel_1 = (
            (in_above_min_vel ** 2 + out_above_min_vel ** 2) / 2
        )
        abs_accel = w_covered_by_abs_accel_1 / w_remaining_after_min_vel

        in_dur = in_above_min_vel / abs_accel
        out_dur = out_above_min_vel / abs_accel
        in_w = (self._in_vel + self._min_vel) / 2 * in_dur
        out_w = (self._min_vel + self._out_vel) / 2 * out_dur

        return [
            _Segment(
                start_t=0.0, stop_t=in_dur,
                a=-abs_accel, b=self._in_vel, c=0.0
            ),
            _Segment(
                start_t=in_dur, stop_t=self._dur - out_dur,
                a=0.0, b=self._min_vel, c=in_w
            ),
            _Segment(
                start_t=self._dur - out_dur, stop_t=self._dur,
                a=abs_accel, b=self._min_vel, c=self._w - out_w
            )
        ]

    def _calculate_segments(self):
        segments = self._calculate_2_segments()
        if segments[1].b >= self._min_vel:
            return segments
        else:
            return self._calculate_3_segments()

    def _get_segment(self, t):
        for segment in self._segments:
            if segment.start_t <= t < segment.stop_t:
                break

        return segment

    def get_x(self, t):
        segment = self._get_segment(t)
        t -= segment.start_t
        a, b, c = segment.a, segment.b, segment.c
        return 0.5*a*t*t + b*t + c


def _get_page_calculators(durations, widths, border_vels):
    return [
        _PageCalculator(dur, w, in_vel, out_vel)
        for dur, w, in_vel, out_vel
        in zip(durations, widths, border_vels[:-1], border_vels[1:])
    ]


class _BorderVelScorer:
    def __init__(self, durations, widths):
        self._durations, self._widths = durations, widths
        self._min_vels_for_borders = self._get_min_vels_for_borders()

    def _get_min_vels_for_borders(self):
        avg_vels_for_pages = self._widths / self._durations
        min_vels_for_pages = avg_vels_for_pages * _MIN_VEL_AS_MUL_OF_AVG_VEL

        min_vels_for_borders = np.empty(
            self._widths.shape[0] + 1, dtype=np.float32
        )
        min_vels_for_borders[0] = min_vels_for_pages[0]
        min_vels_for_borders[-1] = min_vels_for_pages[-1]
        np.maximum(
            min_vels_for_pages[:-1], min_vels_for_pages[1:],
            out=min_vels_for_borders[1:-1]
        )

        return min_vels_for_borders

    def _get_max_accels(self, border_vels):
        return [
            page_calculator.max_abs_accel
            for page_calculator in _get_page_calculators(
                self._durations, self._widths, border_vels
            )
        ]

    def get_scores(self, border_vels):
        border_vels_with_floor = np.maximum(
            border_vels, self._min_vels_for_borders
        )
        scores = np.abs(border_vels - border_vels_with_floor)

        max_accels = self._get_max_accels(border_vels_with_floor)

        for border_vel_i in range(border_vels.shape[0]):
            for offset, score_mul in [
                (-3, 0.5), (-2, 0.7), (-1, 1.0),
                (0, 1.0), (1, 0.7), (2, 0.5)
            ]:
                page_i = border_vel_i + offset
                if 0 <= page_i < len(max_accels):
                    scores[border_vel_i] += max_accels[page_i] * score_mul

        return scores


import cv2
class _Displayer:
    # delete this when completed

    DISPLAY_H, DISPLAY_W = 400, 1000

    def __init__(self, durations, widths):
        self.canvas = np.empty(
            (self.DISPLAY_H, self.DISPLAY_W), dtype=np.uint8
        )

        self.stop_times = np.cumsum(durations)
        self.start_times = self.stop_times - durations
        self.dur = self.stop_times[-1]
        self.stop_xs = np.cumsum(widths)
        self.start_xs = self.stop_xs - widths
        self.w = self.stop_xs[-1]

    def display(self, border_vels):
        self.canvas.fill(255)

        t = 0.0
        for start_t, stop_t, start_x, stop_x, in_vel, out_vel in zip(
            self.start_times, self.stop_times, self.start_xs, self.stop_xs,
            border_vels[:-1], border_vels[1:]
        ):
            page_dur = stop_t - start_t
            page_w = stop_x - start_x
            calculator = _PageCalculator(page_dur, page_w, in_vel, out_vel)

            display_x = int(
                min(start_t / self.dur * self.DISPLAY_W, self.DISPLAY_W - 1)
            )
            self.canvas[:, display_x] = 128

            display_y = int(
                min(start_x / self.w * self.DISPLAY_H, self.DISPLAY_H - 1)
            )
            self.canvas[-1 - display_y] = 128

            while t < stop_t:
                x = start_x + calculator.get_x(t - start_t)

                display_x = int(
                    min(t / self.dur * self.DISPLAY_W, self.DISPLAY_W - 1)
                )
                display_y = int(
                    min(x / self.w * self.DISPLAY_H, self.DISPLAY_H - 1)
                )
                self.canvas[-1 - display_y, display_x] = 0

                t += self.dur / self.DISPLAY_W / 5

        cv2.imshow('', self.canvas)
        cv2.waitKey(0)


def calculate(durations, widths):
    scorer = _BorderVelScorer(durations, widths)
    displayer = _Displayer(durations, widths)

    border_vels = find_vals(len(durations) + 1, scorer, displayer)

    page_calculators = [
        _PageCalculator(dur, w, in_vel, out_vel)
        for dur, w, in_vel, out_vel in zip(
            durations, widths, border_vels[:-1], border_vels[1:]
        )
    ]

    return page_calculators, border_vels[0], border_vels[-1]
